from django.shortcuts import render , HttpResponseRedirect
from django.contrib.auth import authenticate , login , logout ,update_session_auth_hash

from .forms import signupForm ,loginForm ,PostForm
from django.contrib import messages
from .models import Post
from django.contrib.auth.models import Group

def home(request):
    posts=Post.objects.all()
    return render(request , 'myblog/home.html',{'posts':posts} )
def about(request):
    return render(request , 'myblog/about.html')
def contact(request):
    return render(request , 'myblog/contact.html')
def dashboard(request):
    if request.user.is_authenticated :
        posts=Post.objects.all()
        user=request.user
        full_name = user.get_full_name()
        gps=user.groups.all()
        id=request.session.get('id')
        if id :

            return render(request , 'myblog/dashboard.html',{'posts':posts,'full_name':full_name, 'groups':gps ,'id':id})
        else:
            HttpResponseRedirect('/login/')
    else:
        return HttpResponseRedirect('/login/')
def signup(request):
    if request.method=='POST':
        form = signupForm(request.POST)
        if form.is_valid():
            messages.success(request , 'congrats ! now you are an author on our site')
            user=form.save()
            group=Group.objects.get(name='Author')
            user.groups.add(group)
    else:
        form=signupForm()

    return render(request , 'myblog/singup.html' ,{'form':form})
def user_logout(request):
    del request.session['id']
    request.session.clear_expired()
    logout(request)
    return HttpResponseRedirect("/")
def user_login(request):
    if not request.user.is_authenticated:
        if request.method=='POST':
            fm=loginForm(request=request , data=request.POST)
            if fm.is_valid():
                uname=fm.cleaned_data['username']
                pwd=fm.cleaned_data['password']
                user=authenticate(username=uname , password=pwd)
                if user is not None:
                    request.session['id'] = user.id
                    request.session.set_expiry(180)
                    login(request ,user)
                    
                    messages.success(request , "logged in successfully")
                    return HttpResponseRedirect('/dashboard/')
        else:
            fm=loginForm()

        return render (request , 'myblog/login.html',{'form':fm})
    else:
        return HttpResponseRedirect('/dashboard/')
    
# Create your views here.
def delete_post(request , id):
    if request.method=='POST':
        pi = Post.objects.get(pk=id)
        pi.delete()
        return HttpResponseRedirect('/dashboard/')
def update_post(request , id):
    if request.method == 'POST':
        pi=Post.objects.get(pk=id)
        fm=PostForm(request.POST ,instance=pi)
        if fm.is_valid():
            messages.success(request , "your post is successfully updated")
            fm.save()
            return HttpResponseRedirect('/dashboard/')
            
    else:
        pi=Post.objects.get(pk=id)
        fm=PostForm(instance=pi)
    return render(request , 'myblog/update_post.html' ,{'form':fm})

def add_post(request):
    if request.user.is_authenticated:
        if request.method=='POST':
            form=PostForm(request.POST)
            if form.is_valid():
                title=form.cleaned_data['title']
                desc=form.cleaned_data['desc']
                pst=Post(title=title , desc=desc)
                messages.success(request ,"your Post successfully  submitted")
                pst.save()
                form=PostForm()
        else:
            form=PostForm()
        return render(request , 'myblog/add_post.html',{'form':form})
    else:
        return HttpResponseRedirect('/login/')