from django.urls import path 
from . import views

urlpatterns = [
  
    path('' , views.home , name='home'),
    path('about/' , views.about , name='about'),
    path('contact/' , views.contact , name='contact'),
    path('dashboard/' , views.dashboard , name='dashboard'),
    path('signup/' , views.signup , name='signup'),
    path('login/' , views.user_login , name='login'),
    path('logout/' , views.user_logout , name='logout'),
    path('addPost/',views.add_post,name='add_post'),
    path('updatePost/<int:id>/',views.update_post,name='update_post'),
    path('deletePost/<int:id>/',views.delete_post,name='delete_post'),
    
]